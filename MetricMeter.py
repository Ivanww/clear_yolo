class MetricMeter(object):
    def __init__(self):
        super(MetricMeter, self).__init__()
        self.cached_data = None

    def accumulate(self, outputs, targets, k=None):
        raise NotImplementedError()

    def evaluate(self):
        raise NotImplementedError()

    def clear(self):
        raise NotImplementedError()
