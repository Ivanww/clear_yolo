import torch
from tqdm import tqdm

from models import YOLOv3
from dataloader import load_test_data
from utils import YOLOPreProcess
from utils import YOLOPostProcess
from utils import YOLOAnchorHandler
from MAPMeter import AveragePrecisionMeter

test = load_test_data()
model = YOLOv3({'backbone': 'darknet53'}, 80)
model.load_darknet_weights('./weights/yolov3.weights')
model = model.cuda()

preprocess = YOLOPreProcess(feas_dims=416).cuda()
postprocess = YOLOPostProcess(0.5, 0.4, 80).cuda()

anchor_handler = YOLOAnchorHandler(max_feat_size=416).cuda()
meter = AveragePrecisionMeter(80, use_VOC07=False)
meter.initialize()

model.eval()

for batch in tqdm(test):
    batch = preprocess('cuda:0', batch)
    batch = anchor_handler(batch)

    outs = model(batch)
    detections = postprocess(outs, batch)
    for i, d in enumerate(detections):
        boxes = {'boxes': d[:, :4].cpu().numpy(),
            'scores': d[:, 5].cpu().numpy(),
            'labels': d[:, 6].long().cpu().numpy()}

        t_labels = {
            'boxes': batch['boxes'][i].cpu().numpy() * batch['feat_size'].item(),
            'labels': batch['labels'][i].cpu().numpy(),
        }

        meter.accumulate(boxes, t_labels)
mAP = meter.evaluate()
print(mAP)
print(f'mean AP: {mAP[mAP>=0].mean():.4f}')
