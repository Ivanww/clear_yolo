import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import math
import numpy as np

DEFAULT_YOLO_ANCHORS = [
    [(116,90),  (156,198),  (373,326)],
    [(30,61),  (62,45),  (59,119)],
    [(10,13),  (16,30),  (33,23)]
    ]
DEFAULT_YOLO_STRIDES = [13, 26, 52] # 416
DEFAULT_YOLO_SCALES = [32, 16, 8]

YOLO_V3_STRIDES = [32, 16, 8]

def center_offsets(max_feat_size, stride):
    cell_size = max_feat_size // stride
    offsetx = torch.arange(cell_size).repeat(cell_size, 1).view(
        [1, 1, cell_size, cell_size]).float()
    offsety = torch.arange(cell_size).repeat(cell_size, 1).t().view(
        [1, 1, cell_size, cell_size]).float()

    return offsetx, offsety

class YOLOAnchorHandler(nn.Module):
    def __init__(self, anchors=DEFAULT_YOLO_ANCHORS,
        strides=YOLO_V3_STRIDES, max_feat_size=608):
        super(YOLOAnchorHandler, self).__init__()
        self.strides = strides
        self.max_feat_size = max_feat_size

        base_anchors = torch.from_numpy(np.array(anchors)).float()
        for i in range(len(self.strides)):
            base_anchors[i] /= self.strides[i]
        # scale, batch, num_anchor, S, S, 2
        base_anchors = base_anchors.view(len(self.strides), 1, -1, 1, 1, 2)
        self.register_buffer('base_anchors', base_anchors)
        self.offsetx = []
        self.offsety = []
        for s in self.strides:
            x, y = center_offsets(max_feat_size, s)
            self.offsetx.append(x)
            self.offsety.append(y)

    def forward(self, batch):
        feat_size = batch['feat_size']
        device = self.base_anchors.device
        offsets = []
        for i, s in enumerate(self.strides):
            cell_size = feat_size // s
            X, Y = center_offsets(feat_size, s)
            # batch, num_anchor, S, S, 5
            X = X.view(1, 1, cell_size, cell_size)
            Y = Y.view(1, 1, cell_size, cell_size)
            offsets.append((X.to(device), Y.to(device)))

        batch.update({
            'anchors': self.base_anchors.clone(),
            'offsets': offsets,
            'strides': self.strides})
        return batch


if __name__ == "__main__":
    handler = YOLOAnchorHandler()
    anchors = handler(0)
    print(anchors)
