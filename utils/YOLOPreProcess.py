import torch
import torch.nn as nn
import torch.nn.functional as F

# YOLO requires box coordinates to be 'CXCY'
# images, boxes -> resize and reshape
# COCO default XYWH
class YOLOPreProcess(nn.Module):
    def __init__(self, feas_dims=608, pad_value=0., multiscale_interval=10):
        super(YOLOPreProcess, self).__init__()

        self.p_value= pad_value
        if not isinstance(feas_dims, list):
            self.feas_dims = torch.tensor([feas_dims]).long()
        else:
            self.feas_dims = torch.tensor(feas_dims).long()

        self.multiscale_interval = multiscale_interval
        self.normalized_box = True

        self.feat_size = self._choose_feat_size()

        self.batch_count = 0

    def _choose_feat_size(self):
        return self.feas_dims[torch.randperm(self.feas_dims.size(0))[0]].item()

    def _resize_image(self, device, images, feas_dim):
        scaled = []
        pads = []
        sizes = []
        for img in images:
            h, w = img.shape[-2:]
            dim_diff = abs(h - w)
            pad1, pad2 = dim_diff // 2, dim_diff - dim_diff //2
            # PAD = [X1, X2, Y1, Y2], last dimension goes first
            pad = (pad1, pad2, 0, 0) if h >= w else (0, 0, pad1, pad2)
            square_img = torch.nn.functional.pad(img, pad,
                mode='constant', value=self.p_value)

            # use align corner = False to be consistency with openCV
            scaled_img = F.interpolate(square_img.unsqueeze(0), feas_dim,
                mode='nearest')

            pads.append(pad)
            scaled.append(scaled_img)
            sizes.append((w, h))

        scaled = torch.cat(scaled, dim=0).to(device)

        return scaled, pads, sizes

    def _box_coord_transform(self, device, batch_boxes, pads, sizes):
        scaled_boxes = []
        # size are (W, H)
        for boxes, pad, size in zip(batch_boxes, pads, sizes):
            scaled = boxes.clone() # CXCY
            square = size[0] + pad[0] + pad[1]
            # (W, H)
            x1 = size[0] * (scaled[:, 0] - scaled[:, 2] / 2)
            y1 = size[1] * (scaled[:, 1] - scaled[:, 3] / 2)
            x2 = size[0] * (scaled[:, 0] + scaled[:, 2] / 2)
            y2 = size[1] * (scaled[:, 1] + scaled[:, 3] / 2)
            # Adjust for added padding
            x1 += pad[0]
            y1 += pad[2]
            x2 += pad[1]
            y2 += pad[3]
            # Returns (x, y, w, h) -> normalized
            scaled[:, 0] = ((x1 + x2) / 2) / square
            scaled[:, 1] = ((y1 + y2) / 2) / square
            scaled[:, 2] *= size[0] / square
            scaled[:, 3] *= size[1] / square

            scaled_boxes.append(scaled.to(device))

        return scaled_boxes

    def forward(self, device, batch):
        images = batch['images']
        boxes = batch.get('boxes', None)

        # choose dim for this batch
        scaled_images, pads, sizes = self._resize_image(device,
            images, self.feat_size)

        if boxes is not None:
            boxes = self._box_coord_transform(device, boxes, pads, sizes)

        batch.update({'images': scaled_images, 'boxes': boxes,
            'sizes': sizes, 'pads': pads, 'feat_size': self.feat_size})

        self.batch_count += 1
        if self.batch_count == self.multiscale_interval:
            self.batch_count = 0
            self.feat_size = self._choose_feat_size()

        return batch

