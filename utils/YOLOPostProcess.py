import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.transforms import ToTensor
import numpy as np
from PIL.Image import Image
from skimage.transform import resize

def bbox_iou(box1, box2):
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[:, 0], box1[:, 1], box1[:, 2], box1[:, 3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[:, 0], box2[:, 1], box2[:, 2], box2[:, 3]

    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)

    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1 + 1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1 + 1, min=0
    )
    b1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
    b2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)

    iou = inter_area / (b1_area + b2_area - inter_area + 1e-16)

    return iou
def bbox_cxcy_xyxy(boxes):
    xyxy = torch.zeros_like(boxes)
    xyxy[..., 0:2] = boxes[..., 0:2] - boxes[..., 2:4] / 2
    xyxy[..., 2:4] = boxes[..., 0:2] + boxes[..., 2:4] / 2

    return xyxy

def non_max_suppression(prediction, conf_thres=0.5, nms_thres=0.4):
    batch_size = prediction.size(0)
    num_class = prediction.size(-1) - 5# num_class + 5
    prediction = prediction.view(batch_size, -1, num_class+5)


    prediction[..., :4] = bbox_cxcy_xyxy(prediction[..., :4])

    detections = [None] * prediction.size(0)

    for i in range(batch_size):
        conf_mask = prediction[i, :, 4] > conf_thres
        if conf_mask.sum() == 0:
            detections[i] = torch.zeros(0, 7)
            continue

        detections[i] = []

        image_detection = prediction[i, conf_mask, :]
        cls_scores = image_detection[:, 5:].max(1)[0]
        scores = image_detection[:, 4] * image_detection[:, 5:].max(1)[0]
        image_detection = image_detection[(-scores).argsort(), :]
        cls_conf, cls_preds = image_detection[:, 5:].max(1, keepdim=True)
        detected_boxes = torch.cat([image_detection[:, :5], cls_conf,
            cls_preds.float()], dim=-1)

        while detected_boxes.size(0) > 0:
            ious = bbox_iou(detected_boxes[0, :4].unsqueeze(0),
                detected_boxes[:, :4])
            nms_mask = ious > nms_thres
            cls_mask = detected_boxes[0, -1] == detected_boxes[:, -1]

            # merging weights instead of discarding
            same_obj_mask = nms_mask & cls_mask
            weights = detected_boxes[same_obj_mask, 4:5]

            detected_boxes[0, :4] = (weights * detected_boxes[same_obj_mask, :4]).sum(0) / weights.sum()
            detections[i] += [detected_boxes[0]]
            detected_boxes = detected_boxes[~same_obj_mask]
        detections[i] = torch.stack(detections[i]).cpu()

    return detections

class YOLOPostProcess(nn.Module):
    def __init__(self, obj_thres, nms_thres, num_class):
        super(YOLOPostProcess, self).__init__()
        self.obj_thres = obj_thres
        self.nms_thres = nms_thres
        self.num_class = num_class

    def forward(self, outs, batch):
        offsets = batch['offsets']
        anchors = batch['anchors']
        strides = batch['strides']

        X, Y = outs['x'], outs['y']
        W, H = outs['w'], outs['h']

        batch_size = X[0].size(0)
        conf = torch.cat([o.detach().view(batch_size, -1, 1)
            for o in outs['conf']], dim=1)
        cls_pred = torch.cat([c.detach().view(batch_size, -1, self.num_class)
            for c in outs['cls']], dim=1)

        # recover boxes
        X = [((branch.detach() + offset[0]) * stride).view(batch_size, -1)
            for branch, offset, stride in zip(X, offsets, strides)]
        Y = [((branch.detach() + offset[1]) * stride).view(batch_size, -1)
            for branch, offset, stride in zip(Y, offsets, strides)]
        W = [((branch.detach().exp() * anchor[..., 0]) * stride).view(batch_size, -1)
            for branch, anchor, stride in zip(W, anchors, strides)]
        H = [((branch.detach().exp() * anchor[..., 1]) * stride).view(batch_size, -1)
            for branch, anchor, stride in zip(H, anchors, strides)]

        X = torch.cat(X, dim=1)
        Y = torch.cat(Y, dim=1)
        W = torch.cat(W, dim=1)
        H = torch.cat(H, dim=1)

        boxes = torch.stack([X, Y, W, H], dim=-1) # [..., 4]
        prediction = torch.cat([boxes, conf, cls_pred], dim=-1)

        detections = non_max_suppression(prediction, self.obj_thres, self.nms_thres)

        return detections

