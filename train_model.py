import torch
from tqdm import tqdm

from models import YOLOv3
from dataloader import load_test_data
from dataloader import load_train_data
from loss import YOLOLoss

from utils import YOLOPreProcess
from utils import YOLOPostProcess
from utils import YOLOAnchorHandler

from MAPMeter import AveragePrecisionMeter
from LossMeter import LossMeter


def init_model():
    model = YOLOv3({'backbone': 'darknet53', 'use_head': False}, 80)
    model.load_darknet_weights('./weights/darknet53.conv.74')

    return model


def init_optimizer(model):
    optimizer = torch.optim.Adam(model.parameters())

    return optimizer

def train_model():
    test = load_test_data()
    train = load_train_data()

    model = init_model()
    model = model.cuda()
    optimizer = init_optimizer(model)

    loss_fn = YOLOLoss(80, {'obj_scale': 1, 'noobj_scale': 100})

    feas_dims = [x for x in range(416-32*3, 416+32*3+1, 32)]
    print(f'Use Input Sizes {feas_dims}')
    accu_grad_steps = 2
    print(f'Use accumulated gradient step {accu_grad_steps}')
    train_preprocess = YOLOPreProcess(feas_dims=feas_dims).cuda()

    val_preprocess = YOLOPreProcess(feas_dims=416).cuda()
    val_postprocess = YOLOPostProcess(obj_thres=0.001,
        nms_thres=0.5, num_class=80).cuda()

    anchor_handler = YOLOAnchorHandler(max_feat_size=feas_dims[-1]).cuda()
    meter = AveragePrecisionMeter(80, use_VOC07=False)
    loss_meter = LossMeter()

    meter.clear()
    loss_meter.clear()
    num_epoch = 1

    best_mAP = 0.
    for epoch in range(num_epoch):
        model.train()

        train_bar = tqdm(enumerate(train), total=len(train))
        for i, batch in train_bar:
            batch = train_preprocess('cuda:0', batch)
            batch = anchor_handler(batch)

            outs = model(batch)
            torch.save(outs, 'outs.pth')
            torch.save(batch, 'batch.pth')
            loss = loss_fn(outs, batch)

            loss['loss_total'].backward()

            # accumulate gradients for every 2 epochs
            if (i + epoch * len(train)) % accu_grad_steps == 0:
                optimizer.step()
                optimizer.zero_grad()

            loss_meter.accumulate(loss)
            train_bar.set_description('{}'.format(loss_meter.evaluate()))

        meter.clear()
        with torch.no_grad():
            for batch in tqdm(test):
                batch = val_preprocess('cuda:0', batch)
                batch = anchor_handler(batch)

                outs = model(batch)
                detections = val_postprocess(outs, batch)
                for i, d in enumerate(detections):
                    boxes = {'boxes': d[:, :4].numpy(),
                        'scores': d[:, 5].numpy(),
                        'labels': d[:, 6].long().numpy()}

                    size = batch['sizes'][i] # original image size, (W, H)
                    scale = max(size) / batch['feat_size']

                    t_labels = {
                        'boxes': batch['boxes'][i].cpu().numpy() * batch['feat_size'].item(),
                        'labels': batch['labels'][i].cpu().numpy(),
                    }

                    meter.accumulate(boxes, t_labels)
        mAP = mAP_meter.evaluate()

        if best_mAP < mAP[mAP >= 0].mean():
            best_mAP = mAP[mAP >= 0].mean()
            torch.save({'model': model.state_dict()}, 'best_model.pth')

        tqdm.write('Epoch {}: Validation AP {}'.format(epoch, mAP))
        tqdm.write('Epoch {}: meanAP: {}, best mAP {}'.format(epoch, mAP[mAP >= 0].mean(), best_mAP))

        loss_meter.clear()
        meter.clear()

if __name__ == "__main__":
    train_model()
