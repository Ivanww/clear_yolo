import torch
import torch.nn as nn

from .YOLO import YOLOBase
from .Darknet import Darknet53
from .BackboneAdapter import Darknet53Backbone
from .YOLOLayers import Detector, UpsampleLayer

class YOLOv3(YOLOBase):
    def __init__(self, config, num_classes):
        super(YOLOv3, self).__init__(config, num_classes)

        self.header = nn.ModuleList() # use module list here
        # Transform 1
        self.header.append(nn.Sequential(*[
            nn.Conv2d(1024, 512, 1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(512, 1024, 3, padding=1, bias=False),
            nn.BatchNorm2d(1024),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(1024, 512, 1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(512, 1024, 3, padding=1, bias=False),
            nn.BatchNorm2d(1024),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(1024, 512, 1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.1, inplace=False)
        ]))

        self.header.append(Detector(3, 512, 1024))
        self.header.append(UpsampleLayer(512, 256))

        # Transform 2
        self.header.append(nn.Sequential(*[
            nn.Conv2d(512+256, 256, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(256, 512, 3, padding=1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(512, 256, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(256, 512, 3, padding=1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(512, 256, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.1, inplace=False)
        ]))

        self.header.append(Detector(3, 256, 512))
        self.header.append(UpsampleLayer(256, 128))

        # Transform 3
        self.header.append(nn.Sequential(*[
            nn.Conv2d(256+128, 128, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(128, 256, 3, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(256, 128, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(128, 256, 3, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.1, inplace=False),
        ]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(256, 128, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=False)
        ]))
        self.header.append(Detector(3, 128, 256))

    def _build_backbone(self, backbone):
        if callable(backbone):
            self.log.info('Using a callable object as feature extractor')
            self.backbone = backbone
            self.features = self.backbone
            return

        if backbone == 'darknet53':
            self.backbone = Darknet53Backbone()
            self.features = self.backbone.features
        else:
            raise ValueError('')

    def forward(self, batch):
        images = batch['images']

        outs = []
        xs = self.backbone(images)
        x = xs[2]
        feas_idx = 1

        for i, m in enumerate(self.header):
            if isinstance(m, Detector):
                outs.append(m(x))
            elif isinstance(m, UpsampleLayer):
                x = m(x)
                x = torch.cat([x, xs[feas_idx]], dim=1)
                feas_idx -= 1
            else:
                x = m(x)

        # num_anchor = anchor.size(0)
        outputs = {k: [] for k in ['x', 'y', 'w', 'h', 'conf', 'cls']}
        for out in outs:
            num_grid = out.size(2)
            out = out.view(out.size(0), -1, 5+self.num_classes,
                num_grid, num_grid).permute(0, 1, 3, 4, 2).contiguous()

            outputs['x'].append(torch.sigmoid(out[..., 0]))  # Center x
            outputs['y'].append(torch.sigmoid(out[..., 1]))  # Center y
            outputs['w'].append(out[..., 2])  # Width
            outputs['h'].append(out[..., 3])  # Height
            outputs['conf'].append(torch.sigmoid(out[..., 4]))  # Conf
            outputs['cls'].append(torch.sigmoid(out[..., 5:]))  # Cls pred.

        return outputs

if __name__ == "__main__":
    model = YOLOv3({'backbone': 'darknet53'}, 80)
    model.load_darknet_weights('/Users/ivanw/workspace/YLv3/PyTorch-YOLOv3/weights/yolov3.weights')

    x = torch.randn(1, 3, 416, 416)
    outs = model(x)
    print([o.shape for o in outs])
