import torch
import torch.nn as nn

from .Darknet import Darknet53

class Darknet53Backbone(Darknet53):
    def __init__(self):
        super(Darknet53Backbone, self).__init__(1000)

        self.features = self.modulelist

        del self.avg
        del self.linear

        self.scale1 = nn.Sequential(*self.modulelist[:15])
        self.scale2 = nn.Sequential(*self.modulelist[15:24])
        self.scale3 = nn.Sequential(*self.modulelist[24:])

    def forward(self, x):
        outs = []
        outs.append(self.scale1(x))
        outs.append(self.scale2(outs[-1]))
        outs.append(self.scale3(outs[-1]))

        return outs

if __name__ == "__main__":
    x = torch.randn(1, 3, 416, 416)
    m = Darknet53Backbone()
    outs = m(x)
    print([o.shape for o in outs])
