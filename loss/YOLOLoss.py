import torch
import torch.nn as nn
import torch.nn.functional as F

import logging, sys

def bbox_wh_iou(wh1, wh2):
    w1, h1 = wh1[..., 0], wh1[..., 1]
    w2, h2 = wh2[..., 0], wh2[..., 1]
    inter_area = torch.min(w1, w2) * torch.min(h1, h2)
    union_area = (w1 * h1 + 1e-16) + w2 * h2 - inter_area
    return inter_area / union_area

# build target for single branch
class YOLOTargetBuilder(nn.Module):
    def __init__(self, num_class, low_quality_thres=0.5):
        super(YOLOTargetBuilder, self).__init__()
        self.num_class = num_class
        self.low_quality_thres = low_quality_thres

    def forward(self, t_boxes, t_labels, anchors, stride, feat_dim):
        # init t
        device = t_boxes.device
        anchors = anchors.view(-1, 2)
        num_anchor = len(anchors)
        cell_size = feat_dim // stride

        obj_mask = torch.zeros(num_anchor, cell_size, cell_size,
            device=device).long()
        noobj_mask = torch.ones(num_anchor, cell_size, cell_size,
            device=device).long()
        box_tensor = torch.zeros(num_anchor, cell_size, cell_size, 4,
            device=device).float()
        cls_tensor = torch.zeros(num_anchor, cell_size, cell_size,
            self.num_class, device=device).long()

        t_boxes = t_boxes * float(cell_size)
        t_wh = t_boxes[:, 2:]

        ious = bbox_wh_iou(t_wh.unsqueeze(1), anchors.unsqueeze(0))
        _, best_anchor_idx = ious.max(dim=1)

        best_anchors = anchors[best_anchor_idx, :]
        boxwh = torch.log(t_boxes[:, 2:4] / best_anchors)
        boxctr = t_boxes[:, 0:2] - torch.floor(t_boxes[:, 0:2])
        box_offsets = torch.floor(t_boxes[:, 0:2]).long()

        obj_mask[best_anchor_idx, box_offsets[:, 1], box_offsets[:, 0]] = 1
        noobj_mask[best_anchor_idx, box_offsets[:, 1], box_offsets[:, 0]] = 0
        for i in range(ious.size(0)):
            high_quality = ious[i, :] > self.low_quality_thres
            if high_quality.sum() > 0:
                noobj_mask[high_quality, box_offsets[i, 1], box_offsets[i, 0]] = 0

        t_box_tensor = torch.cat([boxctr, boxwh], dim=1)
        box_tensor[best_anchor_idx, box_offsets[:, 1],
            box_offsets[:, 0], :] = torch.cat([boxctr, boxwh], dim=1)

        t_labels = t_labels.long()
        cls_tensor[best_anchor_idx, box_offsets[:, 1], box_offsets[:, 0], t_labels] = 1.

        return box_tensor, cls_tensor, obj_mask, noobj_mask

class YOLOLoss(nn.Module):
    def __init__(self, num_class, coefficients):
        super(YOLOLoss, self).__init__()

        self.num_class = num_class
        self.mse_loss = nn.MSELoss(reduction='mean')
        self.bce_loss = nn.BCEWithLogitsLoss(reduction='mean')
        self.ce_loss = nn.CrossEntropyLoss(reduction='mean')

        self.obj_scale = coefficients['obj_scale']
        self.noobj_scale = coefficients['noobj_scale']

        self.t_builder = YOLOTargetBuilder(num_class)

    def forward(self, outs, batch):
        offsets = batch['offsets']
        anchors = batch['anchors']
        strides = batch['strides']

        X, Y = outs['x'], outs['y']
        W, H = outs['w'], outs['h']

        batch_size = X[0].size(0)
        conf = torch.cat([o.view(batch_size, -1, 1)
            for o in outs['conf']], dim=1).view(-1)
        cls_pred = torch.cat([c.view(batch_size, -1, self.num_class)
            for c in outs['cls']], dim=1).view(-1, self.num_class)

        # recover boxes
        X = torch.cat([branch.view(batch_size, -1) for branch in X], dim=1)
        Y = torch.cat([branch.view(batch_size, -1) for branch in Y], dim=1)
        W = torch.cat([branch.view(batch_size, -1) for branch in W], dim=1)
        H = torch.cat([branch.view(batch_size, -1) for branch in H], dim=1)
        boxes = torch.stack([X, Y, W, H], dim=-1) # [..., 4]
        boxes = boxes.view(-1, 4)


        # return three masks
        t_boxes = batch['boxes']
        t_labels = batch['labels']
        anchors = batch['anchors']
        strides = batch['strides']
        feat_dim = batch['feat_size']

        # batch masks
        batch_boxes, batch_cls, batch_obj_mask, batch_noobj_mask = [], [], [], []
        for t_box, t_label in zip(t_boxes, t_labels):
            # scale_masks
            scale_boxes, scale_cls, scale_obj_mask, scale_noobj_mask = [], [], [], []
            for anchor, stride in zip(anchors, strides):
                scale_box, _cls, obj_mask, noobj_mask = self.t_builder(
                    t_box, t_label, anchor, stride, feat_dim)
                scale_boxes.append(scale_box.view(-1, 4))
                scale_cls.append(_cls.view(-1, 1))
                scale_obj_mask.append(obj_mask.view(-1, 1))
                scale_noobj_mask.append(noobj_mask.view(-1, 1))
            batch_boxes.append(torch.cat(scale_boxes, dim=0))
            batch_cls.append(torch.cat(scale_cls, dim=0))
            batch_obj_mask.append(torch.cat(scale_obj_mask, dim=0))
            batch_noobj_mask.append(torch.cat(scale_noobj_mask, dim=0))
        batch_boxes = torch.stack(batch_boxes, dim=0).view(-1, 4)
        batch_cls = torch.stack(batch_cls, dim=0).view(-1, self.num_class)
        batch_obj_mask = torch.stack(batch_obj_mask, dim=0).view(-1)
        batch_noobj_mask = torch.stack(batch_noobj_mask, dim=0).view(-1)
        batch_conf = batch_obj_mask.float()
        batch_cls = batch_cls.float()

        loss_x = self.mse_loss(boxes[batch_obj_mask, 0], batch_boxes[batch_obj_mask, 0])
        loss_y = self.mse_loss(boxes[batch_obj_mask, 1], batch_boxes[batch_obj_mask, 1])
        loss_w = self.mse_loss(boxes[batch_obj_mask, 2], batch_boxes[batch_obj_mask, 2])
        loss_h = self.mse_loss(boxes[batch_obj_mask, 3], batch_boxes[batch_obj_mask, 3])
        loss_obj = self.bce_loss(conf[batch_obj_mask], batch_conf[batch_obj_mask])
        loss_cls = self.bce_loss(cls_pred[batch_obj_mask], batch_cls[batch_obj_mask])
        loss_noobj = self.bce_loss(conf[batch_noobj_mask],
            batch_conf[batch_noobj_mask])

        # print('Coord Loss X', loss_x)
        # print('Coord Loss Y', loss_x)
        # print('Coord Loss W', loss_x)
        # print('Coord Loss H', loss_x)
        loss_coord = loss_x + loss_y + loss_w + loss_h
        # print('Class Loss ', loss_cls)
        loss_conf = loss_obj * self.obj_scale + loss_noobj * self.noobj_scale
        # print('Conf Loss ', loss_conf)
        loss_total = loss_conf + loss_coord + loss_cls
        loss = {
            'x': loss_x, 'y': loss_y, 'w': loss_w, 'h': loss_h,
            'loss_obj': loss_obj, 'loss_noobj': loss_noobj,
            'loss_cls': loss_cls, 'loss_total': loss_total
        }

        return loss

if __name__ == "__main__":
    batch = torch.load('../batch.pth')
    outs = torch.load('../outs.pth')

    loss_fn = YOLOLoss(80, {'obj_scale': 1, 'noobj_scale': 1})

    loss = loss_fn(outs, batch)
    print(loss)
