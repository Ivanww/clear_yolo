import glob
import random
import os
import sys
import numpy as np
from PIL import Image
import torch
import torch.nn.functional as F

from torch.utils.data import Dataset
import torchvision.transforms as transforms
import torchvision

class ListDataset(Dataset):
    def __init__(self, list_path):
        with open(list_path, "r") as file:
            self.img_files = file.readlines()

        self.label_files = [
            path.replace("images", "labels").replace(".png", ".txt").replace(".jpg", ".txt")
            for path in self.img_files
        ]
        self.max_objects = 100

    def __getitem__(self, index):
        img_path = self.img_files[index % len(self.img_files)].rstrip()

        # Extract image as PyTorch tensor
        img = transforms.ToTensor()(Image.open(img_path).convert('RGB'))

        # Handle images with less than three channels
        if len(img.shape) != 3:
            img = img.unsqueeze(0)
            img = img.expand((3, img.shape[1:]))

        label_path = self.label_files[index % len(self.img_files)].rstrip()
        if os.path.exists(label_path):
            labels = torch.from_numpy(np.loadtxt(label_path).reshape(-1, 5)).float()
        else:
            labels = torch.zeros(0, 5).float()

        return {'images': img, 'boxes': labels, 'paths':img_path}

    def __len__(self):
        return len(self.img_files)

def collate_fn(batch):
    collated = {}
    collated['images'] = [b['images'] for b in batch]
    collated['paths'] = [b['paths'] for b in batch]
    collated['boxes'] = [b['boxes'][:, 1:] for b in batch]
    collated['labels'] = [b['boxes'][:, 0] for b in batch]

    return collated

def load_test_data():
    path = '/home/ivan/data/COCO/5k.txt'
    dataset = ListDataset(path)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=8, shuffle=False, num_workers=1,
        collate_fn=collate_fn
    )

    return dataloader

def load_train_data():
    path = '/home/ivan/data/COCO/trainvalno5k.txt'
    dataset = ListDataset(path)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=2, shuffle=True, num_workers=2,
        collate_fn=collate_fn
    )

    return dataloader
def coco_collect_fn(batch):
    collated = {}
    collated['images'] = [transforms.ToTensor()(b[0].convert('RGB')) for b in batch]

    boxes = []
    labels = []
    for b in batch:
        box = [torch.from_numpy(np.array(a['bbox'])) for a in b[1]]
        label = np.array([a['category_id'] for a in b[1]])
        boxes.append(torch.stack(box, dim=0))
        labels.append(torch.from_numpy(label))

    collated.update({
        'boxes': boxes, 'labels': labels
    })
    return collated

def load_coco_torch(datadir, batch_size = 1, num_workers=1):
    base = os.path.abspath(datadir)
    train_image_dir = os.path.join(base, 'train2017')
    train_anno_file = os.path.join(base, 'annotations', 'instances_train2017.json')
    val_image_dir = os.path.join(base, 'val2017')
    val_anno_file = os.path.join(base, 'annotations', 'instances_val2017.json')

    train_data = torchvision.datasets.CocoDetection(train_image_dir, train_anno_file)
    val_data = torchvision.datasets.CocoDetection(val_image_dir, val_anno_file)

    train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=False,
        num_workers=num_workers, collate_fn=coco_collect_fn
    )
    val_loader = torch.utils.data.DataLoader(val_data, batch_size=batch_size, shuffle=False,
        num_workers=num_workers, collate_fn=coco_collect_fn
    )

    return train_loader, val_loader
if __name__ == '__main__':
    from utils import YOLOPreProcess
    loader = load_test_data()
    handle = YOLOPreProcess()
    for batch in loader:
        batch = handle('cpu', batch)
        print(batch['images'].shape)
        print(batch['boxes'][0].shape)
        print(batch['labels'])
        print(batch['pads'])
        print(batch['sizes'])
        break
    # datadir = '/home/ivan/data/COCO'
    # train, val = load_coco_torch(datadir)
    # for batch in train:
    #     print(batch['images'][0].shape)
    #     print(batch['labels'])

